module Messaging
  module Generators
    class ConfigGenerator < Rails::Generators::Base
      desc 'Creates a kw_messaging_client gem configuration file at config/messaging.yml'

      def self.source_root
        @_messaging_source_root ||= File.expand_path("../templates", __FILE__)
      end

      def create_config_file
        template 'messaging.yml', File.join('config', 'messaging.yml')
      end

      # def create_initializer_file
      #   template 'initializer.rb', File.join('config', 'initializers', 'messaging.rb')
      # end
    end
  end
end