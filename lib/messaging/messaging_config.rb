module Messaging
  module MessagingConfig

    def self.messaging_url
      messaging_config = Messaging::MessagingConfig.get_config
      
      if messaging_config['service'] == 'faye'
        protocol = messaging_config['ssl'] == true ? 'https' : 'http'
        "#{protocol}://#{messaging_config['url']}:#{messaging_config['port']}/faye"
      elsif messaging_config['service'] == 'ably'
        ""
      else
        raise "Unknown service for messaging: #{messaging_config['service']}"
      end
    end
    
    def self.service_lib
      messaging_config = Messaging::MessagingConfig.get_config
      
      if messaging_config['service'] == 'faye'
        "MessagingFaye"
      elsif messaging_config['service'] == 'ably'
        "MessagingAbly"
      else
        raise "Unknown service for messaging: #{messaging_config['service']}"
      end
    end
    
    def self.api_key
      messaging_config = Messaging::MessagingConfig.get_config
      
      if messaging_config['service'] == 'faye'
        ""
      elsif messaging_config['service'] == 'ably'
        messaging_config['api_key']
      else
        raise "Unknown service for messaging: #{messaging_config['service']}"
      end
    end
    
    def self.get_config
      begin
        yml = YAML.safe_load(ERB.new(File.read('config/messaging.yml')).result, [Symbol], [], true)
        return yml[Rails.env]
      rescue
        raise 'You must create messaging.yml'
      end
    end
    
  end
end