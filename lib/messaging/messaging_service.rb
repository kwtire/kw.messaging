module Messaging
  module MessagingService
    
    def self.publish(channel, publication, message)
      messaging_config = Messaging::MessagingConfig.get_config
      
      if messaging_config['service'] == 'faye'
        Messaging::MessagingFaye.publish channel, publication, message
      elsif messaging_config['service'] == 'ably'
        Messaging::MessagingAbly.publish messaging_config['api_key'], channel, publication, message
      else
        raise "Unknown messaging service: #{messaging_config['service']}"
      end
    end
    
  end
end