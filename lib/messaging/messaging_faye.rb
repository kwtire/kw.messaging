module Messaging
  module MessagingFaye
    
    def self.publish(channel, publication, message)
      message = {
        channel: "/#{channel}/#{publication}",
        data: message
      }
      
      url = Messaging::MessagingConfig.messaging_url
      uri = URI.parse url
      
      # https
      if url.starts_with? 'https'
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = true
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        request = Net::HTTP::Post.new(uri.request_uri)
        request.form_data = { message: message.to_json }
        response = http.request(request)
        
      # http
      else
        Net::HTTP.post_form uri, message: message.to_json
      end      
    end
    
  end
end