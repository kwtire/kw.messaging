module Messaging
  module MessagingAbly
    require 'ably-rest'
    
    def self.publish(api_key, channel, publication, message)
      client = Ably::Rest.new(key: api_key)
      ch = client.channel channel
      ch.publish publication, message
    end
    
  end
end