module Messaging
  class MessagingHelper
    
    def self.messaging_data_div
      "<div id=\"kw_messaging_client_data\" 
            data-messaging-lib=\"#{Messaging::MessagingConfig.service_lib}\" 
            data-messaging-url=\"#{Messaging::MessagingConfig.messaging_url}\" 
            data-messaging-api-key=\"#{Messaging::MessagingConfig.api_key}\" 
            style=\"display:none;\">
      </div>".html_safe
    end
    
  end
end