# KwMessagingClient

This contains the implementation to connect to the kw.messaging service.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'kw_messaging_client', git: "https://TsG0woaXZSDjyrzFExPrYQn7XdZkEsWN:TsG0woaXZSDjyrzFExPrYQn7XdZkEsWN@bitbucket.org/hmconsulting/kw.messaging.git"
```

And then execute:

    $ bundle

To generate the messaging.yml, run:

	$ rails g messaging:config

The service types available are: `faye`

## Usage

Include this in any erb that needs to connect to the messaging service.  The easiest way is to include it in the layout.

```rails
<%= Messaging::MessagingHelper.messaging_data_div %>
```

The messaging javascript library includes two methods for communicating:  `subscribe` and `publish`.

To subscribe to a publication:

```javascript
Messaging.subscribe({
	channel: <channel_name>, 
	publication: <publication_name>,
	connect: function() {
		// executes when subscribtion is successful
	},
	success: function(data) {
		// executes when subscription receives a published message
	},
	error: function() {
		// executes when there is a connection problem
	}
});
```

To publish a message:

```javascript
Messaging.publish({
	channel: <channel_name>,
	publication: <publication_name>,
	message: <message_hash>,
	success: function(data) {
		// executes when the message was published successfully
	},
	error: function() {
		// executes when there is a connection problem
	}
});
```

To publish a message on the server:

```ruby
Messaging::MessagingService.publish <channel_name>, <publication_name>, <message_hash>
```

## Development

After checking out the repo, run `bin/setup` to install dependencies.

To release a new version, update the version number in `version.rb`, then commit and push to git.

To update the gem in your project execute:

	$ bundle update kw_messaging_client