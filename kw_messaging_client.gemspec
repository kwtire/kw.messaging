# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'kw_messaging_client/version'

Gem::Specification.new do |spec|
  spec.name          = "kw_messaging_client"
  spec.version       = KwMessagingClient::VERSION
  spec.authors       = ["Justin Harrell"]
  spec.email         = ["jharrell@hmcon.com"]

  spec.summary       = %q{Client for subscribing and publishing messages to kw.messaging service.}
  #spec.description   = %q{TODO: Write a longer description or delete this line.}
  spec.homepage      = "https://bitbucket.org/hmconsulting/kw.messaging/src/master/"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib","vendor"]

  if spec.respond_to?(:metadata)
    #spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com' to prevent pushes to rubygems.org, or delete to allow pushes to any server."
  end

  spec.add_development_dependency "bundler", "~> 1.8"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rails", "4.0.0"
  spec.add_dependency "ably-rest", "1.0.6"
end
