var MessagingAbly = {};
(function() {
	var ably;
	
	this.publish = function(settings) {
		ably = getAbly(settings);
		ably.connection.on('failed', function() {
			settings.error();
		});

		//console.log('publish to ' + settings.channel + ':' + settings.publication);
		var channel = ably.channels.get(settings.channel);
		channel.publish(settings.publication, settings.message, function(error) {
			if (error) {
				settings.error(error);
			} else {
				settings.success();
			}
		});
	};
	
	this.subscribe = function(settings) {
		ably = getAbly(settings);
		ably.connection.on('connected', function() {
			settings.connect();
		});
		ably.connection.on('failed', function() {
			settings.error();
		});
		
		//console.log('subscribe to ' + settings.channel + ':' + settings.publication);
		var channel = ably.channels.get(settings.channel);
		var subscription = {
			api_key: settings.api_key,
			channel: settings.channel,
			publication: settings.publication,
			listener: function(message) {
				settings.success(message.data);
			}
		};
		channel.subscribe(settings.publication, subscription.listener);
		
		return subscription;
	};
	
	this.unsubscribe = function(subscription) {
		ably = getAbly(subscription);
		
		var channel = ably.channels.get(subscription.channel);
		channel.unsubscribe(subscription.publication, subscription.listener);
	};
	
	
	function getAbly(settings) {
		if (!ably) {
			return new Ably.Realtime(settings.api_key);
		}			
			
		return ably;
	}

}).apply(MessagingAbly);