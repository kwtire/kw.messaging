var Messaging = {

	publish: function(options) {
		var settings = $.extend({
			url: Messaging.messagingUrl(),
			api_key: Messaging.messagingApiKey(),
			success: function(){},
			error: function(){}
		}, options);
		
		window[Messaging.messagingLib()].publish(settings);
	},
	
	subscribe: function(options) {
		var settings = $.extend({
			url: Messaging.messagingUrl(),
			api_key: Messaging.messagingApiKey(),
			connect: function(){},
			success: function(){},
			error: function(){}
		}, options);
		
		return window[Messaging.messagingLib()].subscribe(settings);
	},
	
	unsubscribe: function(subscription) {
		window[Messaging.messagingLib()].unsubscribe(subscription);
	},
	
	messagingLib: function() {
		return $('#kw_messaging_client_data').data('messaging-lib');
	},
	
	messagingUrl: function() {
		return $('#kw_messaging_client_data').data('messaging-url');
	},
	
	messagingApiKey: function() {
		return $('#kw_messaging_client_data').data('messaging-api-key');
	}
};