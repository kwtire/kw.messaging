var MessagingFaye = {

	publish: function(settings) {
		var client = new Faye.Client(settings.url, { timeout: 60, retry: 5 });
		client.on('transport:down', function() {
			settings.error();
		});
		
		//console.log('publish to ' + '/' + settings.channel + '/' + settings.publication);
		var publication = client.publish('/' + settings.channel + '/' + settings.publication, settings.message);
		publication.then(function() {
			settings.success();
		}, function(error) {
			settings.error(error);
		});
	},
	
	subscribe: function(settings) {
		var client = new Faye.Client(settings.url, { timeout: 60, retry: 5 });
		client.on('transport:down', function() {
			settings.error();
		});

		//console.log('subscribe to ' + '/' + settings.channel + '/' + settings.publication);
		var subscription = client.subscribe('/' + settings.channel + '/' + settings.publication, function(message) {
			settings.success(message);
		});
		subscription.then(function() { settings.connect(); });
		
		return subscription;
	},
	
	unsubscribe: function(subscription) {
		subscription.cancel();
	}
	
};